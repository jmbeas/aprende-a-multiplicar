require 'timeout'

def mensaje(s)
  puts s
  system 'espeak.exe -v spanish "'+s+'"'
end

def obtiene_respuesta
  puts "(Responde 'salir' para salir)"
  respuesta = ""
  begin
    respuesta = Timeout::timeout(5) {
      gets.chomp
    }
  rescue Timeout::Error
    mensaje "Meeec. Respuesta incorrecta. Vuelve a empezar."
  end
  return respuesta
end

mensaje "Examen de matemáticas"
fin = false
respuestas_correctas = 0
limite_superfenomenal = 2
while !fin
  a = Random.new.rand(3..9).to_i
  b = Random.new.rand(3..9).to_i
  respuesta_correcta = false
  while !respuesta_correcta
    mensaje "¿Cuánto es "+a.to_s+" por "+b.to_s+"?"
    respuesta = obtiene_respuesta
    if respuesta.upcase == "SALIR" then
      fin = true
      break
    end
    if respuesta == (a * b).to_s then
      respuesta_correcta = true
      respuestas_correctas = respuestas_correctas + 1
      if respuestas_correctas == limite_superfenomenal then
        mensaje_superfenomenal = ""
        (1..limite_superfenomenal-1).each { mensaje_superfenomenal += "Super "}
        mensaje_superfenomenal += "Fenomenal"
        mensaje mensaje_superfenomenal
        respuestas_correctas = 0
        limite_superfenomenal = limite_superfenomenal + 1
      else
        mensaje "Fenomenal"
        puts "Te faltan "+(limite_superfenomenal-respuestas_correctas).to_s+" respuestas correctas"
      end
    else
      mensaje "Oh, has fallado. Vuelve a intentarlo"
      respuestas_correctas = 0
    end
  end
end
