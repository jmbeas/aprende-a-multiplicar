# README #

Este programita lo hemos ido creando niño2 y yo para ayudarle a memorizar las tablas de multiplicar.

Consiste en un bucle sin fin que va preguntando con voz sintetizada (usa eSpeak) diferentes multiplicaciones y jaleando con un Fenomenal cuando se acierta. Se ha gamificado añadiendo un Super en cada ciclo de 100% de respuestas completas. Cada ciclo va incrementando la dificultad requiriendo una respuesta correcta más que el anterior.
